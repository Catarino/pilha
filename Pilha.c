#include<stdio.h>
#include<stdlib.h>
#define MAX 7

int pilhaVazia(int posicao){
	getchar();
	if(posicao == -1)
		return 0;
	else
		return -1;
}
int pilhaCheia(int posicao){
	if(posicao == (MAX - 1))
		return 0;
	else
		return -1;
}
int empilha(int valor[],int posicao){
	int numero;
	printf("Digite o numero:	");
	scanf("%d", &numero);
	if(pilhaCheia(posicao) == 0)
		return -1;
	posicao++;
	valor[posicao] = numero;
	return 0;
}
int desempilha(int posicao){
	if(pilhaVazia(posicao) == 0)
		return -1;
	posicao--;
	return 0;
}
void menu(){
	printf("=================================\n");
	printf("1)	EMPILHAR\n");
	printf("2)	DESEMPILHAR\n");
	printf("3)	MOSTRAR\n");
	printf("0)	SAIR\n");
	printf("=================================\n");
}
void mostrar(int posicao, int valor[]){
	int i;
	printf("=================================\n");
	for(i = 0; i <= posicao; i++){
		printf("%d\n",valor[i]);
	}
	printf("=================================\n");
}
int main(){
	int valor[MAX];
	int posicao = -1;
	int opcao = 1;
	do{
		menu();
		scanf("%d",&opcao);
		switch(opcao){
			case 1:
				if(empilha(valor,posicao) == 0){
					printf("Empilhado.\n");
					posicao++;
				}
				else
					printf("Nao Empilhado. \"Pilha Cheia\"\n");
				break;
			case 2:
				if(desempilha(posicao) == 0){
						posicao--;
						printf("Desempilhado.\n");
					}
				else
					printf("Nao Desempilhado. \"Pilha Vazia\"\n");
				break;
			case 3:
				if(pilhaVazia(posicao) == 0)
					printf("Pilha Vazia\n");
				else
					mostrar(posicao,valor);
				break;
		}
	}while(opcao != 0)
	return 0;
}
