#include<stdio.h>
#include<stdlib.h>
#define MAX 7

int filaCheia(int posicao){
	if(posicao == (MAX -1))
		return 0;
	else
		return -1;
}
int filaVazia(int posicao){
	if(posicao == -1)
		return 0;
	else
		return -1;		
}
int incluiFila(int posicao, int valor[]){
	int numero;
	printf("Digite o numero: ");
	scanf("%d",&numero);
	if(filaCheia(posicao) == 0){
		printf("Fila Cheia.\n");
		return posicao;
	}
	else{
		posicao++;
		valor[posicao] = numero;
		printf("Numero cadastrado.\n");
		return posicao;
	}
}
int removeFila(int posicao, int valor[]){
	int i = 0;
	if(filaVazia(posicao) == 0){
		printf("Lista Vazia.\n");
		return posicao;
	}
	else{
		for(i = 0 ; i <= posicao && i != MAX; i++){
			valor[i] = valor[i+1];
		}
		posicao--;
		return posicao;
	}
}
void mostrar(int posicao, int valor[]){
	int i;
	printf("=================================\n");
	for(i = 0; i <= posicao; i++){
		printf("%d\n",valor[i]);
	}
	printf("=================================\n");
}
void menu(){
	printf("=================================\n");
	printf("1)	INCLUIR NA FILA\n");
	printf("2)	RETIRAR DA FILA\n");
	printf("3)	MOSTRAR\n");
	printf("0)	SAIR\n");
	printf("=================================\n");
}
int main(){
	int valor[MAX];
	int posicao = -1;
	int opcao;
	do{
		menu();
		scanf("%d",&opcao);
		switch(opcao){
			case 1:
				posicao = incluiFila(posicao, valor);
				break;
			case 2:
				posicao = removeFila(posicao, valor);
				break;
			case 3:
				if(filaVazia(posicao) == 0){
					printf("Lista Vazia.\n");
				}
				else
					mostrar(posicao, valor);
				break;
		}
	}while(opcao != 0);
	return 0;
}
